module gitlab.com/lightmeter/go-postfix-socketmap

go 1.20

require (
	github.com/rs/zerolog v1.31.0
	github.com/seandlg/netstring v1.0.0
	github.com/smartystreets/goconvey v1.8.1
)

require (
	github.com/gopherjs/gopherjs v1.17.2 // indirect
	github.com/jtolds/gls v4.20.0+incompatible // indirect
	github.com/mattn/go-colorable v0.1.13 // indirect
	github.com/mattn/go-isatty v0.0.19 // indirect
	github.com/smarty/assertions v1.15.0 // indirect
	golang.org/x/sys v0.12.0 // indirect
)
