# Go Postfix Socketmap

This repository provides a Go implementation of the Postfix Socketmap table lookup client, as specified on:
[https://www.postfix.org/socketmap_table.5.html](https://www.postfix.org/socketmap_table.5.html).

The supported transports are TCP and Unix Domain Sockets.

This implementation is incomplete and not very performant, but has been used in production at Lightmeter,
so I consider it "production ready".

## LICENSE

Apache 2.0

## TODO

- Implement automated CI/CD, code coverage reports, automated fuzzing, etc. 
- Support multiple maps per socket (referred as "name" by Postfix).
- Document it properly.
- Improve test coverage.
