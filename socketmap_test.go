// SPDX-FileCopyrightText: 2023 Lightmeter <hello@lightmeter.io>
//
// SPDX-License-Identifier: Apache License 2.0

package socketmap

import (
	"bytes"
	"errors"
	"fmt"
	"io"
	"testing"

	"github.com/seandlg/netstring"
	. "github.com/smartystreets/goconvey/convey"
)

type fakeMap struct {
	queries []string
}

func (m *fakeMap) Answer(q string) (ResponseCode, string, error) {
	m.queries = append(m.queries, q)

	switch q {
	case `not found at all!`:
		return NotFound, "look for something else", nil
	case `should err`:
		return PermError, "", fmt.Errorf(`I err, therefore I am`)
	}

	return OK, "got " + q, nil
}

type readWriter struct {
	responses bytes.Buffer
	requests  *bytes.Reader
}

func (rw *readWriter) Write(p []byte) (n int, err error) {
	n, err = rw.responses.Write(p)

	//log.Info().Msgf(`Wrote response: '%s'`, string(p))

	return
}

func (rw *readWriter) Read(p []byte) (n int, err error) {
	if len(p) == 0 {
		return 0, nil
	}

	// on reading comma, be sure that only one byte is read, simulating
	// a "blocking" operation, as there are no bytes left.

	n, err = rw.requests.Read(p[:1])
	if err != nil && errors.Is(err, io.EOF) {
		return 0, err
	}

	if err != nil {
		panic(err)
	}

	if p[0] == ',' {
		//log.Info().Msg(`Read comma! returning now!`)
		return 1, nil
	}

	n, err = rw.requests.Read(p[1:])

	if err == nil {
		// in addition to the comma
		n++
	}

	//log.Info().Msgf(`Read request: '%s'`, string(p))

	return
}

func concat(bs ...[]byte) []byte {
	r := []byte{}

	for _, b := range bs {
		r = append(r, b...)
	}

	return r
}

func TestLightmap(t *testing.T) {
	Convey("Handling requests", t, func() {
		m := fakeMap{}

		Convey("OK", func() {
			requests := concat(
				netstring.MarshalFrom([]byte(`example gimme`)),
				netstring.MarshalFrom([]byte(`invalid`)),
				netstring.MarshalFrom([]byte(`another invalid command`)),
				netstring.MarshalFrom([]byte(`example not found at all!`)),
				netstring.MarshalFrom([]byte(`example should err`)),
				netstring.MarshalFrom([]byte(`example test123`)),
			)

			rw := readWriter{requests: bytes.NewReader(requests)}

			err := handleRequests("example", &rw, &m, 41)
			So(err, ShouldBeNil)

			So(m.queries, ShouldResemble, []string{"gimme", "not found at all!", "should err", "test123"})

			So(rw.responses.Bytes(), ShouldResemble, concat(
				netstring.MarshalFrom([]byte(`OK got gimme`)),
				netstring.MarshalFrom([]byte(`PERM Invalid query: "invalid"`)),
				netstring.MarshalFrom([]byte(`PERM Expected first part to be "example", but got "another"`)),
				netstring.MarshalFrom([]byte(`NOTFOUND `)),
				netstring.MarshalFrom([]byte(`PERM I err, therefore I am`)),
				netstring.MarshalFrom([]byte(`OK got test123`)),
			))
		})

		Convey(`Invalid input`, func() {
			Convey(`Shorter string. This is an edge case during the tests, so it'll simply be ignored`, func() {
				rw := readWriter{requests: bytes.NewReader([]byte(`09:000000000,7:0000000,23:00000000000000000000000,2100000000:0`))}
				err := handleRequests("example", &rw, &m, 41)
				So(err, ShouldBeNil)
			})

			Convey(`Invalid syntax 1`, func() {
				rw := readWriter{requests: bytes.NewReader([]byte(`A0`))}
				err := handleRequests("example", &rw, &m, 41)
				So(err, ShouldNotBeNil)
				So(err, ShouldWrap, netstring.Garbled)
			})

			Convey(`Invalid syntax 2`, func() {
				rw := readWriter{requests: bytes.NewReader([]byte(`9:000000000,7:0000000,2300000021:000000000000000000000,00000000000`))}
				err := handleRequests("example", &rw, &m, 41)
				So(err, ShouldBeNil)
			})

			Convey(`Invalid syntax 3`, func() {
				rw := readWriter{requests: bytes.NewReader([]byte(`09:000000000,7:0000000,23:000000000:0000000000000,2100000000:0`))}
				err := handleRequests("example", &rw, &m, 41)
				So(err, ShouldBeNil)
			})
		})
	})
}

func FuzzLightmap(f *testing.F) {
	requests := concat(
		netstring.MarshalFrom([]byte(`example gimme`)),
		netstring.MarshalFrom([]byte(`invalid`)),
		netstring.MarshalFrom([]byte(`another invalid command`)),
		netstring.MarshalFrom([]byte(`example not found at all!`)),
		netstring.MarshalFrom([]byte(`example should err`)),
		netstring.MarshalFrom([]byte(`example test123`)),
	)

	f.Add(requests)

	f.Fuzz(func(t *testing.T, requests []byte) {
		m := fakeMap{}
		rw := readWriter{requests: bytes.NewReader(requests)}

		err := handleRequests("example", &rw, &m, 41)
		if err == nil {
			return
		}

		if !errors.Is(err, netstring.Garbled) && !errors.Is(err, netstring.Incomplete) && !errors.Is(err, io.EOF) {
			t.Errorf(`Failed: %v`, err)
		}
	})
}
