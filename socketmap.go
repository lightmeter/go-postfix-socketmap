// SPDX-FileCopyrightText: 2023 Lightmeter <hello@lightmeter.io>
//
// SPDX-License-Identifier: Apache License 2.0

package socketmap

import (
	"bytes"
	"errors"
	"fmt"
	"io"
	"net"
	"os"
	"strings"

	"github.com/rs/zerolog/log"
	"github.com/seandlg/netstring"
)

type Map interface {
	Answer(string) (ResponseCode, string, error)
}

func ListenOnAddress(name, addr string, m Map) error {
	listener, err := net.Listen("tcp", addr)
	if err != nil {
		return err
	}

	return Listen(name, listener, m)
}

func ListenOnFile(name, filename string, m Map) error {
	// remove socket file if it exists
	if _, err := os.Stat(filename); err == nil {
		return err
	}

	listener, err := net.Listen("unix", filename)
	if err != nil {
		return err
	}

	// enable anyone to write in the socket, an easy way to enable the chrooted postfix to access it
	if err := os.Chmod(filename, 0777); err != nil {
		return err
	}

	return Listen(name, listener, m)
}

func Listen(name string, listener net.Listener, m Map) error {
	// TODO: pass context for cancellation!
	i := uint64(0)

	for {
		i++

		conn, err := listener.Accept()
		if err != nil {
			return err
		}

		go func(i uint64) {
			defer conn.Close()

			if err := handleRequests(name, conn, m, i); err != nil {
				log.Printf(`Could not handle requests: %v`, err)
			}
		}(i)
	}

	return nil
}

func handleRequests(name string, conn io.ReadWriter, m Map, i uint64) error {
	j := 0

	var responseBuffer bytes.Buffer

	for {
		responseBuffer.Reset()

		j++

		request := netstring.ForReading()

		err := request.ReadFrom(conn)
		if err != nil && errors.Is(err, netstring.Incomplete) {
			// Nothing else to read. Finish here.
			return nil
		}

		if err != nil {
			return err
		}

		query, err := request.Bytes()
		if err != nil {
			return err
		}

		code, answer := func() (ResponseCode, string) {
			code, answer, err := answer(name, string(query), m)

			if err != nil {
				return code, err.Error()
			}

			return code, answer
		}()

		components := []string{string(code), ` `}

		// NotFound does not replies with any messages
		if code != NotFound {
			components = append(components, answer)
		}

		// it should be okay to write each of them individually, as they are bufferized
		for _, c := range components {
			responseBuffer.Write([]byte(c))
		}

		// TODO: we can improve performance by writing the responseBuffer direct to the connection,
		// thus avoiding allocating a []byte unnecessarily.
		response := netstring.MarshalFrom(responseBuffer.Bytes())

		if _, err := io.Copy(conn, bytes.NewReader(response)); err != nil {
			return err
		}
	}
}

type ResponseCode string

const (
	OK           ResponseCode = "OK"
	NotFound     ResponseCode = "NOTFOUND"
	PermError    ResponseCode = "PERM"
	TempError    ResponseCode = "TEMP"
	TimeoutError ResponseCode = "TIMEOUT"
)

// https://www.postfix.org/socketmap_table.5.html
func answer(name, query string, m Map) (ResponseCode, string, error) {
	index := strings.Index(query, " ")

	if index < 0 {
		return PermError, "", fmt.Errorf(`Invalid query: "%s"`, query)
	}

	cmd := query[:index]

	if cmd != name {
		return PermError, "", fmt.Errorf(`Expected first part to be "%s", but got "%s"`, name, cmd)
	}

	return m.Answer(query[index+1:])
}
